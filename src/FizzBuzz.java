// Classe Fizz
//
// teste
public class FizzBuzz {

	public String validaNumero (int numero) {

		if(restoPor3(numero) == 0 && restoPor5(numero) == 0) {
			return "FizzBuzz";
		} 

		if(restoPor3(numero) == 0){
			return "Fizz";
		}

		if(restoPor5(numero) == 0)
		{
			return "Buzz";
		}

		return Integer.toString(numero);

	}

	public int restoPor5(int numero) {
		return (numero % 5);
	}

	public int restoPor3(int numero) {
		return (numero % 3);
	}
}
